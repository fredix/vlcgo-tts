# vlcgo-tts

vlcgo-tts is a fork of [htgo-tts](https://github.com/hegedustibor/htgo-tts)  
it use [libvlc-go](https://github.com/adrg/libvlc-go) instead of mplayer.


### Install
```
go get "framagit.org/fredix/vlcgo-tts"
```

### Update
```
go get -u "framagit.org/fredix/vlcgo-tts"
```

### Remove
```
go clean -i "framagit.org/fredix/vlcgo-tts"
```

### Import
```go
import "framagit.org/fredix/vlcgo-tts"
```

### Use
```go
speech := vlcgotts.Speech{Folder: "audio", Language: "en"}
speech.Speak("Your sentence.")
```
